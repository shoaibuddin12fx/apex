// material.module.ts

import { NgModule } from '@angular/core';
import { MatInputModule, MatSelectModule, MatIconModule } from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatSnackBarModule} from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';

import {
  MatButtonModule,
  MatCardModule
} from '@angular/material';

@NgModule({
  imports: [
    MatButtonModule,
    MatCardModule,
    MatInputModule, 
    MatButtonModule,
    MatSelectModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
  ],
  exports: [
    MatButtonModule,
    MatCardModule,
    MatInputModule, 
    MatButtonModule,
    MatSelectModule,
    MatIconModule
  ]
})

export class MaterialModule {}