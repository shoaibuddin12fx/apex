import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilityService, NetworkService, LocalVariablesService } from 'app/shared/_helpers';
import { HotkeysService, Hotkey } from 'angular2-hotkeys';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';

@Component({
  selector: 'app-add-table',
  templateUrl: './add-table.component.html',
  styleUrls: ['./add-table.component.scss']
})
export class AddTableComponent implements OnInit {

  @Input() id;
  @Input() iparams: any = {}
  @Input() type: string = 'Add';
  main_node: string = '';
  title: string = '';
  icon: string = '';
  departments: any = [];
  @Input() asModal: boolean = false;
  @Output() modalClose: EventEmitter<any> = new EventEmitter();

  aForm: FormGroup;
  submitted = false;

  constructor(private router: Router,
    public formBuilder: FormBuilder,
    public utility: UtilityService,
    public network: NetworkService,
    public storage: LocalVariablesService,
    private _hotkeysService: HotkeysService,
    public route: ActivatedRoute) {

      
      this.utility.saveShortcutKey(this);
      this.utility.cancelShortcutKey(this);
      this.nextFieldShortcutKey();

  }

  ngOnInit() {
    
    if(this.asModal == false){
      this.main_node = this.route.snapshot.url[0]['path'];
      this.type = this.route.snapshot.url[1]['path'];
    }

    if(this.type == 'edit'){
      if(this.asModal == false){
        this.id = this.route.snapshot.url[2]['path'];
      } 
    }

    console.log(this.main_node, this.type);

    this.getIparams(this.utility.capitalize(this.type))
    this.setupForm();
    
  }

  setupForm() {

    this.aForm = this.formBuilder.group({
      data: this.formBuilder.array([
        this.getRow()
      ])
    });

    if(this.main_node == 'tables'){
      this.getList('departments').then( res_1 => {
        this.departments = res_1['dept'];
      })
    }

    if(this.id){
      this.getItemById(this.id)
    }

  }

  getRow(){
    const res_id = this.utility.restaurantId();

    switch(this.main_node){
      case 'departments':
        return this.formBuilder.group({
          restaurant_id: [res_id, Validators.compose([Validators.required]) /*, VemailValidator.checkEmail */ ],
          dept_name: ['', Validators.compose([Validators.required, Validators.maxLength(100), Validators.pattern(/^[a-zA-Z0-9 ]*$/)])],
          is_default: ['0'],
          status: ['1']
          
        })
        
        break;
      case 'tables':
        return this.formBuilder.group({
          restaurant_id: [res_id, Validators.compose([Validators.required]) ],
          rest_dept_id: ['', Validators.compose([Validators.required]) ],
          table_no: ['', Validators.compose([Validators.required])]
        });
        break;
      case 'portions':
        return this.formBuilder.group({
          restaurant_id: [res_id, Validators.compose([Validators.required]) /*, VemailValidator.checkEmail */ ],
          portion_name: ['', Validators.compose([Validators.required, Validators.maxLength(100), Validators.pattern(/^[a-zA-Z0-9_ ]*$/)])],
          is_default: ['0'],
        })
        break;
      case 'addons':
        return this.formBuilder.group({
          restaurant_id: [res_id, Validators.compose([Validators.required]) /*, VemailValidator.checkEmail */ ],
          addons: ['', Validators.compose([Validators.required, Validators.maxLength(100), Validators.pattern(/^[a-zA-Z0-9_ ]*$/)])],
          value: [0, Validators.compose([Validators.required, Validators.maxLength(100), Validators.pattern(/^[0-9]*$/)])],
        })
        break;
      case 'toppings':
        return this.formBuilder.group({
          restaurant_id: [res_id, Validators.compose([Validators.required]) /*, VemailValidator.checkEmail */ ],
          topping_name: ['', Validators.compose([Validators.required, Validators.maxLength(100), Validators.pattern(/^[a-zA-Z0-9_ ]*$/)])],
          value: [0, Validators.compose([Validators.required, Validators.maxLength(100), Validators.pattern(/^[0-9]*$/)])],
        })
        break;
      default:
        break;
    }

    
  }

  // add new row
  addRow() {
    const control = <FormArray>this.aForm.controls['data'];
    control.push(this.getRow());
  }

  // remove row
  removeRow(i: number) {
    const control = <FormArray>this.aForm.controls['data'];
    control.removeAt(i);
  }

  get formData() { return <FormArray>this.aForm.get('data'); }
  get f() { return this.aForm.controls; }

  // On submit button click
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.aForm.invalid) {
        return;
    }

    var formdata = this.aForm.value;
    for(var i = 0; i < formdata['data'].length; i++){
      var temp = formdata['data'][i]['is_default'];
      temp = (temp == true || temp == '1' ) ? '1' : (temp == false || temp == '0' ) ? '0' : '0';
      formdata['data'][i]['is_default'] = temp;

      var temp2 = formdata['data'][i]['status'];
      temp2 = (temp2 == true || temp2 == '1' ) ? '1' : (temp2 == false || temp2 == '0' ) ? '0' : '0';
      formdata['data'][i]['status'] = temp2;
      
    }

    if(this.id){
      var formdata = this.aForm.value['data'][0];
      
      formdata['id'] = this.id;
      console.log(formdata);
      this.editTable(formdata);
    }else{
      this.addTable(this.aForm.value);
    }

    

    


  }

  addTable(data) {

    var self = this;
    swal("Add Entry", "Are You Sure you want to add new enteries?", "question").then( (value) => {
      if(value['value'] == true){

        this.network.addList(this.main_node, data).subscribe( data => {
          this.showSuccessMessage(data)
          this.submitted = false;
          this.aForm.reset();
          this.viewDismiss();
        }, err => {
          console.error(err)
          this.submitted = false;
        })

      }
    });

  }

  editTable(data) {

    this.network.setItem(this.main_node, data).subscribe( data => {
      this.showSuccessMessage(data)
      this.submitted = false;
      this.aForm.reset();
      this.viewDismiss();
    }, err => {
      console.log(err)
      this.submitted = false;
    })

  }

  showSuccessMessage(data){
    console.log(data);
    if(!data) return;
    if(data['success'] == "true"){
      this.utility.presentSuccessToast(data['message'])
    }
  }

  getItemById(id) {

    this.network.getItem(this.main_node, {id: id}).subscribe( data => {

      console.log(data);
      const firstRow = (<FormArray>this.aForm.controls['data']).at(0);

      switch(this.main_node){
        case 'departments':
          firstRow['controls']['dept_name'].setValue(data['dept']['dept_name'])
          const defaultVal = (data['dept']['is_default'] == '1') ? true : false;
          firstRow['controls']['is_default'].setValue(defaultVal)
          const defaultStatus = (data['dept']['status'] == '1') ? true : false;
          firstRow['controls']['status'].setValue(defaultStatus)
          break;
        case 'tables':
          firstRow['controls']['rest_dept_id'].setValue(data['table']['rest_dept_id'])
          firstRow['controls']['table_no'].setValue(data['table']['table_no'])

          break;
        case 'portions':
          firstRow['controls']['portion_name'].setValue(data['portion']['portion_name'])
          const porDefault = (data['portion']['is_default'] == '1') ? true : false;
          firstRow['controls']['is_default'].setValue(porDefault) 
          break;
        case 'addons':
          firstRow['controls']['addons'].setValue(data['addon']['addons'])
          firstRow['controls']['value'].setValue(data['addon']['value'])
          break;
        case 'toppings':
          firstRow['controls']['topping_name'].setValue(data['topping']['topping_name'])
          firstRow['controls']['value'].setValue(data['topping']['value'])
          break;
        default:
          break;
      }


    }, err => {})

  }

  alphanumbers50($event){

    var temp = $event.target.value;
    temp = (temp).replace(/[^a-zA-Z0-9]/gi, '').substring(0, 50);
    $event.target.value = temp;

  }

  alphanumbersWithSpace50($event){

    var temp = $event.target.value;
    temp = (temp).replace(/[^a-zA-Z0-9 ]/gi, '').substring(0, 50);
    $event.target.value = temp;

  }

  onCancel() {
    this.viewDismiss();
  }

  getIparams(type){
    switch(this.main_node){
      case 'departments':
          this.title = type + ' Department';
          this.icon = 'ft-box';
        break;
      case 'tables':
          this.title = type + ' Table';
          this.icon = 'ft-plus-square';
        break;
      case 'portions':
          this.title = type + ' Portion';
          this.icon = 'ft-grid'
        break;
      case 'addons':
          this.title = type + ' AddOns';
          this.icon = 'ft-radio'
        break;
      case 'toppings':
          this.title = type + ' Toppings';
          this.icon = 'ft-radio'
        break;
      default:
        break;
    }
  }

  


  viewDismiss(){ 
    
    if(this.asModal == false){
      this.router.navigate(['pages/'+this.main_node])
    }else{
      this.modalClose.emit('close');
    }
  }

  getList(node) {

    return new Promise( (resolve, reject) => {
      const res_id = this.utility.restaurantId();
      const res_data = {'restaurant_id': res_id }

      this.network.getList(node, res_data).subscribe( data => {
        resolve(data);
      }, err => { reject(null) })
    })

  }

  nextFieldShortcutKey(){
    this._hotkeysService.add(new Hotkey('enter', (event: KeyboardEvent): boolean => {
      
      return false;
    }, ['INPUT'], 'Send a secret message to the console.'));
  }

  // addToName(data) {

  //   var temp = this.f['dept_name'].value;
  //   temp = (temp + '' + data).substring(0, 6);
  //   console.log(temp);
  //   this.f['dept_name'].setValue(temp);
  // }


  
  // onlynumbers50digit($event) {
  //   const val = $event.target.value;
  //   $event.target.value = ("" + val).replace(/^[0-9a-zA-Z ]+$/g, '').slice(-50);
  // }


    


  // @HostListener allows us to also guard against browser refresh, close, etc.
  @HostListener('window:beforeunload')
  canDeactivate(): Observable<boolean> | boolean {
    // insert logic to check if there are pending changes here;
    // returning true will navigate without confirmation
    // returning false will show a confirm dialog before navigating away
    return this.submitted == false && !this.aForm.dirty; // insert your code here for submit event

  }


}

