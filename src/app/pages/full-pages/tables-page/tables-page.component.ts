import { filter } from 'rxjs/operators';
import { AddTableComponent } from './add-table/add-table.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import {NetworkService, LocalVariablesService, UtilityService} from 'app/shared/_helpers';
import {ActivatedRouteSnapshot, ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, ReactiveFormsModule, FormsModule} from '@angular/forms';

import {NgSelectModule, NgOption} from '@ng-select/ng-select';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import {Hotkey, HotkeysService} from 'angular2-hotkeys';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';

@Component({
  selector: 'app-tables-page',
  templateUrl: './tables-page.component.html',
  styleUrls: ['./tables-page.component.scss']
})
export class TablesPageComponent implements OnInit {

  title;
  admin_edit_layout = '0';
  res_data: any;
  plist: any = [];
  _plist: any = [];
  main_node = '';
  departments: any = [];
  _departments: any = [];
  public loader = true;

  @ViewChild(TablesPageComponent) _tablerows: DatatableComponent;

  constructor(public network: NetworkService,
    private router: Router,
    public utility: UtilityService,
    private _hotkeysService: HotkeysService,
    private modalService: NgbModal,
    public route: ActivatedRoute) {

      this.utility.addShortcutKey(this);

    }

  ngOnInit() {


    this.admin_edit_layout = this.utility.isAdminEdit();

    console.log( this.route.snapshot.url[0]['path'] );
    this.main_node = this.route.snapshot.url[0]['path'];
    const title = this.route.snapshot.data['title'];
    if(title){ this.title = title }

    this.initialize()

  }

  initialize(){
    switch(this.main_node){
      case 'departments':

        this.getList('departments').then( res_1 => {
          this.departments = res_1['dept'];
          this._departments = res_1['dept'];
          this.loader = false;
        })

        break;
      case 'tables':

        this.getList('departments').then( res_1 => {
          this.departments = res_1['dept'];
          this._departments = res_1['dept'];
          this.getList('tables').then( res_2 => {
            this.plist = res_2['table'];
            this._plist = res_2['table'];
            this.loader = false;
          })
        })

        break;
      case 'portions':
        this.getList('portions').then( res_1 => {
          this.plist = res_1['portion'];
          this._plist = res_1['portion'];
          this.loader = false;
        })
        break;
      case 'addons':
        this.getList('addons').then( res_1 => {
          this.plist = res_1['addon'];
          this._plist = res_1['addon'];
          this.loader = false;
        })
        break;
      case 'toppings':
        this.getList('toppings').then( res_1 => {
          this.plist = res_1['topping'];
          this._plist = res_1['topping'];
          this.loader = false;
        })
        break;
      case 'categories':
        this.getList('departments').then( res_1 => {
          this.departments = res_1['dept'];
          this._departments = res_1['dept'];
        this.getList('categories').then( res_2 => {

            this.plist = res_2['category'];
            this._plist = res_2['category'];
            this.loader = false;
          })
        })
          break;
      
          default:
        break;
    }
  }

  getList(node) {

    return new Promise( (resolve, reject) => {
      const res_id = this.utility.restaurantId();
      const res_data = {'restaurant_id': res_id }

      this.network.getList(node, res_data).subscribe( data => {
        
        resolve(data);
      }, err => { reject(null) })
    })

  }

  delete(item) {


    swal("Delete", 'Are you sure you want to delete ' + this.getItemMainLabel(item) + " " + this.utility.capitalize(this.main_node) + "?", "question").then( (ret) => {
      console.log(ret);
      if(ret['value'] == true){
        const Id = item['id'];
        if (Id) {
          this.network.delete(this.main_node, {'id': Id}).subscribe( data => {
            console.log(data);
            this.remove(item)
          }, err => {})
        }
      }
    });
    
  }

  remove(item) {
    if(this.main_node == 'departments'){
      this.departments.forEach( (itm, index) => {
        if (itm.id == item.id) { this.departments.splice(index, 1); }
      });
    }else{
      this.plist.forEach( (itm, index) => {
        if (itm.id == item.id) { this.plist.splice(index, 1); }
      });
    }
    
  }


  getDeptNameById(id) {
    const dept = this.departments.filter( x => x.id == id)[0];
    if (dept) {
      return dept['dept_name'];
    } else {
      return '';
    }
  }

  fireAddView(){

    if (this.utility.isAdminEdit() == '1') {
      this.router.navigate(['./add'], {relativeTo: this.route})
    }else{
      const modal = this.modalService.open(AddTableComponent, { size: 'lg', backdrop: 'static' });
      modal.componentInstance.asModal = true;
      modal.componentInstance.main_node = this.main_node;
      modal.componentInstance.modalClose.subscribe( () => {
        modal.dismiss();
        this.initialize();
      })
    }
  }

  fireEditView(id){

    if (this.utility.isAdminEdit() == '1') {
      this.router.navigate(['./edit', id], {relativeTo: this.route})
    }else{
      const modal = this.modalService.open(AddTableComponent, { size: 'lg', backdrop: 'static' });
      modal.componentInstance.asModal = true;
      modal.componentInstance.main_node = this.main_node;
      modal.componentInstance.id = id;
      modal.componentInstance.type = 'edit';
      modal.componentInstance.modalClose.subscribe( () => {
        modal.dismiss();
        this.initialize();
      })
    }
  }

  getItemMainLabel(item){
    switch(this.main_node){
      case 'departments':
        return item['dept_name']
        break;
      case 'tables':
        return item['table_no']
        break;
      case 'portions':
        return item['portion_name']
        break;
      case 'addons':
        return item['addons']
        break;
      case 'toppings':
        return item['topping_name']
        break; 
      default:
        break;
    }
  }

  filterBy($event, column){

    const filterval = (column != 'rest_dept_id') ? $event.target.value.toLowerCase() : $event;
    
    switch(this.main_node){
      case 'departments':
        this.departments = this._departments;
        break;
      case 'tables':
          
        
        if(column == 'table_no'){
          this.plist = (!filterval) ? this._plist : this._plist.filter(x => x[column].toLowerCase().indexOf(filterval) !== -1);
        }

        if(column == 'rest_dept_id'){
          this.plist = (!filterval) ? this._plist : this._plist.filter(x => x[column] == filterval);
        }


        break;
      case 'portions':

          if(column == 'portion_name'){
            this.plist = (!filterval) ? this._plist : this._plist.filter(x => x[column].toLowerCase().indexOf(filterval) !== -1);
          }

        break;
      case 'addons':

          if(column == 'addons'){
            this.plist = (!filterval) ? this._plist : this._plist.filter(x => x[column].toLowerCase().indexOf(filterval) !== -1);
          }
        break;
      case 'toppings':
          
          if(column == 'topping_name'){
            this.plist = (!filterval) ? this._plist : this._plist.filter(x => x[column].toLowerCase().indexOf(filterval) !== -1);
          }
        break; 
      default:
        break;
    }

  }




}
