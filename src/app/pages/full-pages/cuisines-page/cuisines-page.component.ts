import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { UtilityService, NetworkService } from 'app/shared/_helpers';
import { ActivatedRoute, Router } from '@angular/router';
import { HotkeysService, Hotkey } from 'angular2-hotkeys';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';

@Component({
  selector: 'app-cuisines-page',
  templateUrl: './cuisines-page.component.html',
  styleUrls: ['./cuisines-page.component.scss']
})
export class CuisinesPageComponent implements OnInit {

  title;
  admin_edit_layout = '0';
  res_data: any;
  cuisines: any = [];
  _cuisines: any = [];
  retro_cuisines: any = [];
  aForm: FormGroup;
  submitted = false;
  public loader = true;
  
  @ViewChild(CuisinesPageComponent) _tablerows: DatatableComponent;

  constructor(public network: NetworkService,
    public formBuilder: FormBuilder,
    private router: Router,
    public utility: UtilityService,
    private activatedRoute: ActivatedRoute,
    private _hotkeysService: HotkeysService,
    public navParams: ActivatedRoute

  ) {
    
    this.utility.saveShortcutKey(this);
  }

  setupForm() {

    const res_id = this.utility.restaurantId();

    this.aForm = this.formBuilder.group({
      _cuisines: new FormArray([])
    })

  }

  ngOnInit() {

    this.setupForm();
    this.getCuisinesList().then( (data1) => {

      if(data1['success'] == 'true'){
        this.cuisines = data1['cuisines'];
        this._cuisines = data1['cuisines'];
        
        console.log(this.cuisines);
        
        this.getRestroCuisinesList().then( (data2) => {
          console.log(data1, data2);
          if(data2['success'] == 'true'){
            this.retro_cuisines = data2['cuisines'];
            this.updateCheckboxes()
          }else{
            this.addCheckboxes();
          }
          this.loader = false;
        });
      }
    })
  }

  getCuisinesList(){

    return new Promise( (resolve, reject) => {
      const res_id = this.utility.restaurantId();
      this.res_data = {'restaurant_id': res_id }
      this.network.cuisines(this.res_data)
      .subscribe( data => {
        console.log(data);
        //this.departments = data['dept'];
        resolve(data);
      }, err => { reject(false) })
    })

  }

  getRestroCuisinesList(){

    return new Promise( (resolve, reject) => {
      const res_id = this.utility.restaurantId();
      this.res_data = {'restaurant_id': res_id }
      this.network.restroCuisines(this.res_data).subscribe( data => {
        console.log(data);
        //this.departments = data['dept'];
        resolve(data);
      }, err => { reject(false) })
    })

  }

  private addCheckboxes() {
    this.cuisines.map((o, i) => {
      const control = new FormControl(); // if first item set to true, else false
      control.setValue(false);
      (this.aForm.controls._cuisines as FormArray).push(control);
    });
  }

  private updateCheckboxes(){

    this.cuisines.map((o, i) => {
      const control = new FormControl(); // if first item set to true, else false

      const ele = this.retro_cuisines.filter(x => x.cuisine_id == o.id )
      if(ele.length > 0){
        control.setValue(true);
      }else{
        control.setValue(false);
      }
      
      (this.aForm.controls._cuisines as FormArray).push(control);
    });
    
  }

  get formData() { return <FormArray>this.aForm.get('_cuisines'); }


  onSubmit(){

    this.submitted = true;
    console.log(this.aForm.controls._cuisines.value);
    
    var cui_ids = []
    var cuisines_values = this.aForm.controls._cuisines.value;
    cuisines_values.forEach( (element, index) => {
      const _id = this.cuisines[index]['id'];
      if(element == true){
        cui_ids.push({'id': _id})
      }
    });

    const res_id = this.utility.restaurantId();
    const dataObj = {'restaurant_id': res_id, "cuisine": cui_ids }
    console.log(dataObj);

    this.addDept(dataObj)







  }

  addDept(cuis) {

    this.network.addRestroCuisines(cuis).subscribe( data => {
      this.submitted = false;
      console.log(data);
      this.showSuccessMessage(data)
      
    }, err => {})
  }

  showSuccessMessage(data){
    console.log(data);
    if(!data) return;
    if(data['success'] == "true"){
      this.utility.presentSuccessToast(data['message'])
    }
  }


}
