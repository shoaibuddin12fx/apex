import { NetworkService } from './../../../shared/_helpers/network.service';
import { Component, OnInit } from '@angular/core';
import { UtilityService } from 'app/shared/_helpers';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  constructor(public utility: UtilityService,
    public network: NetworkService
    ) { }

  ngOnInit() {
    
  }

  

}
