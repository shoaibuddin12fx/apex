import { value } from './../../../shared/data/dropdowns';
import { Component, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { AuthService } from 'app/shared/auth/auth.service';
import { UtilityService } from 'app/shared/_helpers'

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss']
})

export class LoginPageComponent {

    aForm: FormGroup;
    submitted = false;

    constructor(private router: Router,
        public formBuilder: FormBuilder,
        private authService: AuthService,
        public utility: UtilityService,
        private route: ActivatedRoute) { 

            this.checkIfAlreadyLoggedIn();
            this.setupForm();


    }

    

    checkIfAlreadyLoggedIn(){
        var token = this.authService.isAuthenticated();
        if(token){
            this.router.navigate(['pages/home']);
        }
    }

    setupForm(){

        this.aForm = this.formBuilder.group({
            mobile_no: ['9660554000', Validators.compose([Validators.required, Validators.maxLength(10)]) /*, VemailValidator.checkEmail */ ],
            password: ['123456', Validators.compose([Validators.required, Validators.maxLength(6)])],
            remember_me: [false],
            user_type: ['2']
            
        })

    }

    get f() { return this.aForm.controls; }

    // On submit button click    
    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.aForm.invalid) {
            return;
        }

        console.log(this.aForm.value);
        var formdata = this.aForm.value;
        this.authService.signinUser(formdata).then( (success) => {
            
            this.router.navigate(['pages/home']);
            this.submitted = false;
            this.utility.presentSuccessToast(success["message"]);
        }, err => {
            this.submitted = false;
            // this.utility.presentFailureToast(err["message"]);
            
        })
        

    }




    addToPin(item){
       
        var temp = this.f['password'].value;
        temp = (temp + '' + item).substring(0, 6);
        console.log(temp);
        this.f['password'].setValue(temp);
    }

    clearPin(){
        this.f['password'].setValue('');
    }

    // On Forgot password link click
    onForgotPassword() {
        this.router.navigate(['forgotpassword'], { relativeTo: this.route.parent });
    }
    // On registration link click
    onRegister() {
        this.router.navigate(['register'], { relativeTo: this.route.parent });
    }

    onlynumbers10digit($event){
        const val = $event.target.value;
        $event.target.value = ("" + val).replace(/\D/g, '').slice(-10);
    }

    onlynumbers6digit($event){
        const val = $event.target.value;
        $event.target.value = ("" + val).replace(/\D/g, '').slice(-6);
    }
}