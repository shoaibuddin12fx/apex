import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FullPagesRoutingModule } from './full-pages-routing.module';
import { ChartistModule} from 'ng-chartist';
import { AgmCoreModule } from '@agm/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { GalleryPageComponent } from './gallery/gallery-page.component';
import { InvoicePageComponent } from './invoice/invoice-page.component';
import { HorizontalTimelinePageComponent } from './timeline/horizontal/horizontal-timeline-page.component';
import { HorizontalTimelineComponent } from './timeline/horizontal/component/horizontal-timeline.component';
import { VerticalTimelinePageComponent } from './timeline/vertical/vertical-timeline-page.component';
import { UserProfilePageComponent } from './user-profile/user-profile-page.component';
import { SearchComponent } from './search/search.component';
import { FaqComponent } from './faq/faq.component';
import { KnowledgeBaseComponent } from './knowledge-base/knowledge-base.component';
import { HomePageComponent } from './home-page/home-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TablesPageComponent } from './tables-page/tables-page.component';
import { NgSelectModule, NgOption} from '@ng-select/ng-select';
import { QRCodeModule } from 'angularx-qrcode';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {HotkeyModule} from 'angular2-hotkeys';
import { CuisinesPageComponent } from './cuisines-page/cuisines-page.component';

import { OrdersPageComponent } from './orders-page/orders-page.component';
import { AddTableComponent } from './tables-page/add-table/add-table.component';
import { UiSwitchModule } from 'ngx-ui-switch';

@NgModule({
    entryComponents:[
        AddTableComponent
    ],
    exports:[
        AddTableComponent
    ],
    imports: [
        CommonModule,
        FullPagesRoutingModule,
        FormsModule,
        ChartistModule,
        AgmCoreModule,
        NgbModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule,
        QRCodeModule,
        NgxDatatableModule,
        HotkeyModule,
        UiSwitchModule
    ],
    declarations: [
        GalleryPageComponent,
        InvoicePageComponent,
        HorizontalTimelinePageComponent,
        HorizontalTimelineComponent,
        VerticalTimelinePageComponent,
        UserProfilePageComponent,
        SearchComponent,
        FaqComponent,
        KnowledgeBaseComponent,
        HomePageComponent,
        TablesPageComponent,
        CuisinesPageComponent,
        OrdersPageComponent,
        AddTableComponent,
    ]
})
export class FullPagesModule { }
