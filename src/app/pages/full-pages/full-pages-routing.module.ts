import { AddTableComponent } from './tables-page/add-table/add-table.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GalleryPageComponent } from './gallery/gallery-page.component';
import { InvoicePageComponent } from './invoice/invoice-page.component';
import { HorizontalTimelinePageComponent } from './timeline/horizontal/horizontal-timeline-page.component';
import { VerticalTimelinePageComponent } from './timeline/vertical/vertical-timeline-page.component';
import { UserProfilePageComponent } from './user-profile/user-profile-page.component';
import { SearchComponent } from './search/search.component';
import { FaqComponent } from './faq/faq.component';
import { KnowledgeBaseComponent } from './knowledge-base/knowledge-base.component';
import { HomePageComponent } from './home-page/home-page.component';
import { TablesPageComponent } from './tables-page/tables-page.component';
import { CanDeactivateGuardGuard} from '../../shared/guard/can-deactivate-guard.guard';
import { AdminEditGuard } from '../../shared/guard/admin-edit.guard';
import { CuisinesPageComponent } from './cuisines-page/cuisines-page.component';
import { TncAcceptedGuard } from 'app/shared/guard/tnc-accepted.guard';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'home',
        component: HomePageComponent,
        data: {
          title: 'Home Page'
        },
        canActivate: [TncAcceptedGuard],
      },
      {
        path: 'departments',
        component: TablesPageComponent,
        data: {
          title: 'Departments'
        },
        canActivate: [TncAcceptedGuard],
      },
      {
        path: 'departments/add',
        component: AddTableComponent,
        data: {
          title: 'Add Departments'
        },
        canActivate: [AdminEditGuard, TncAcceptedGuard],
        canDeactivate: [CanDeactivateGuardGuard]
      },
      {
        path: 'departments/edit/:id',
        component: AddTableComponent,
        data: {
          title: 'Edit Departments'
        },
        canActivate: [AdminEditGuard, TncAcceptedGuard],
        canDeactivate: [CanDeactivateGuardGuard]
      },
      {
        path: 'tables',
        component: TablesPageComponent,
        data: {
          title: 'Tables'
        },
        canActivate: [TncAcceptedGuard],
      },
      {
        path: 'tables/add',
        component: AddTableComponent,
        data: {
          title: 'Add Tables'
        },
        canActivate: [AdminEditGuard, TncAcceptedGuard],
        canDeactivate: [CanDeactivateGuardGuard]
      },
      {
        path: 'tables/edit/:id',
        component: AddTableComponent,
        data: {
          title: 'Edit Tables'
        },
        canActivate: [AdminEditGuard, TncAcceptedGuard],
        canDeactivate: [CanDeactivateGuardGuard]
      },
      {
        path: 'cuisines',
        component: CuisinesPageComponent,
        data: {
          title: 'Cuisines'
        },
        canActivate: [TncAcceptedGuard],
      },
      {
        path: 'portions',
        component: TablesPageComponent,
        data: {
          title: 'Portions'
        },
        canActivate: [TncAcceptedGuard],
      },
      {
        path: 'portions/add',
        component: AddTableComponent,
        data: {
          title: 'Add Portions'
        },
        canActivate: [AdminEditGuard, TncAcceptedGuard],
        canDeactivate: [CanDeactivateGuardGuard]
      },
      {
        path: 'portions/edit/:id',
        component: AddTableComponent,
        data: {
          title: 'Edit Portions'
        },
        canActivate: [AdminEditGuard, TncAcceptedGuard],
        canDeactivate: [CanDeactivateGuardGuard]
      },
      {
        path: 'addons',
        component: TablesPageComponent,
        data: {
          title: 'AddOns'
        },
        canActivate: [TncAcceptedGuard],
      },
      {
        path: 'addons/add',
        component: AddTableComponent,
        data: {
          title: 'Add AddOns'
        },
        canActivate: [AdminEditGuard, TncAcceptedGuard],
        canDeactivate: [CanDeactivateGuardGuard]
      },
      {
        path: 'addons/edit/:id',
        component: AddTableComponent,
        data: {
          title: 'Edit AddOns'
        },
        canActivate: [AdminEditGuard, TncAcceptedGuard],
        canDeactivate: [CanDeactivateGuardGuard]
      },
      {
        path: 'toppings',
        component: TablesPageComponent,
        data: {
          title: 'Toppings'
        },
        canActivate: [TncAcceptedGuard],
      },
      {
        path: 'toppings/add',
        component: AddTableComponent,
        data: {
          title: 'Add Toppings'
        },
        canActivate: [AdminEditGuard, TncAcceptedGuard],
        canDeactivate: [CanDeactivateGuardGuard]
      },
      {
        path: 'toppings/edit/:id',
        component: AddTableComponent,
        data: {
          title: 'Edit Toppings'
        },
        canActivate: [AdminEditGuard, TncAcceptedGuard],
        canDeactivate: [CanDeactivateGuardGuard]
      },

      {
        path: 'categories',
        component: TablesPageComponent,
        data: {
          title: 'Categories'
        },
        canActivate: [TncAcceptedGuard],
      },
      {
        path: 'categories/add',
        component: AddTableComponent,
        data: {
          title: 'Add Categories'
        },
        canActivate: [AdminEditGuard, TncAcceptedGuard],
        canDeactivate: [CanDeactivateGuardGuard]
      },
      {
        path: 'categories/edit/:id',
        component: AddTableComponent,
        data: {
          title: 'Edit Tables'
        },
        canActivate: [AdminEditGuard, TncAcceptedGuard],
        canDeactivate: [CanDeactivateGuardGuard]
      },


      // apart from actual links used
      {
        path: 'gallery',
        component: GalleryPageComponent,
        data: {
          title: 'Gallery Page'
        }
      },
      {
        path: 'invoice',
        component: InvoicePageComponent,
        data: {
          title: 'Invoice Page'
        }
      },
      {
        path: 'horizontaltimeline',
        component: HorizontalTimelinePageComponent,
        data: {
          title: 'Horizontal Timeline Page'
        }
      },
      {
        path: 'verticaltimeline',
        component: VerticalTimelinePageComponent,
        data: {
          title: 'Vertical Timeline Page'
        }
      },
      {
        path: 'profile',
        component: UserProfilePageComponent,
        data: {
          title: 'User Profile Page'
        }
      },
      {
        path: 'search',
        component: SearchComponent,
        data: {
          title: 'Search'
        }
      },
      {
        path: 'faq',
        component: FaqComponent,
        data: {
          title: 'FAQ'
        }
      },
      {
        path: 'kb',
        component: KnowledgeBaseComponent,
        data: {
          title: 'Knowledge Base'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FullPagesRoutingModule { }
