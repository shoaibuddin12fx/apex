import { Injectable } from '@angular/core';
import { NetworkService, LocalVariablesService } from 'app/shared/_helpers'
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
  token: string;
  _user: any;
  _permission: any;
  _restro: any;

  constructor(public network: NetworkService, 
    private router: Router,
    public storage: LocalVariablesService) {}

  signupUser(email: string, password: string) {
    // your code for signing up the new user
  }

  signinUser(formdata) {
    // your code for checking credentials and getting tokens for for signing in user
    return new Promise( (resolve, reject) => {
      this.network.loginUser(formdata).subscribe( (data) => {
          if (data['success'] == 'true') {
            this.saveData(data);
            resolve(data);
          } else {
            reject(data);
          }

      }, err => {
          console.log(err);
      })

    })

  }

  saveData(data){
    console.log(data);
    if (data['user'] != null) {
      this._user = data['user'];
      this.storage.set('user', JSON.stringify(data['user']));
      let user_id = data['user']['id'];
      this.token = (user_id != null) ? user_id : null;
      this.storage.set('token', this.token);
    }

    if (data['permission'] != null && data['permission'].length > 0) {
      this._permission = data['permission'];
      this.storage.set('permission', JSON.stringify(data['permission']))
    }

    if (data['restro'] != null && data['restro'].length > 0) {
      data['restro'][0]['admin_edit_layout'] = '0';
      //data['restro'][0]['is_tnc_accepted'] = '0';
      this._restro = data['restro'];
      this.storage.set('restro', JSON.stringify(data['restro']))
    }
  }

  

  logout() {
    this.token = null;
    this.storage.delete('token');
    // window.location.reload();
    this.router.navigate(['login']);
  }

  getToken() {
    return this.token;
  }

  isAuthenticated() {
    // here you can check if user is authenticated or not through his token
    return new Promise( resolve => {
      let id = this.storage.get('token');

      resolve(id);
    })

  }
}
