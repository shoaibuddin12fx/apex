import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    return new Observable<boolean>(observer => {
      this.authService.isAuthenticated().then( token => {

        if (!token) {
          this.router.navigate(['login']);
          return observer.next(false);
        } else {
          return observer.next(true);
        }
        
      });
    });


  }
}
