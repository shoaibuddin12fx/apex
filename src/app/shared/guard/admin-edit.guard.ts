import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from '../auth/auth.service';
import {UtilityService} from '../_helpers';

@Injectable({
  providedIn: 'root'
})
export class AdminEditGuard implements CanActivate {

  constructor(private utility: UtilityService, private router: Router) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    return new Observable<boolean>(observer => {

      if (!(this.utility.isAdminEdit() == '1')) {
        this.router.navigate(['../']);
        return observer.next(false);
      } else {
        return observer.next(true);
      }
    })
  }


}
