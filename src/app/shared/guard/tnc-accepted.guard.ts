import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from '../auth/auth.service';
import {UtilityService, NetworkService} from '../_helpers';

@Injectable({
  providedIn: 'root'
})
export class TncAcceptedGuard implements CanActivate {

  constructor(private utility: UtilityService, 
    private network: NetworkService,
    private auth: AuthService,
    private router: Router) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    return new Observable<boolean>(observer => {
      this.network.isTncAccepted().then( response => {
        const _response: any = response;
        if(_response['dismiss'] == 'cancel'){
          this.auth.logout();
        }
      }, err => {
        console.error(err)
      });
      return observer.next(true);
    })
  }


}
