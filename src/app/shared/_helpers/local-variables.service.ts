import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalVariablesService {

  keylist = 'pik-keys'
  constructor() { }

  get(key){
    return localStorage.getItem(key);
  }

  set(key, value){
    localStorage.setItem(key, value);
  }

  delete(key){
    localStorage.removeItem(key);
  }

  addKeyList(key){
      
  }

  getKeyList(){

  }

  removeKeyList(){

  }

}
