import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import {LocalVariablesService} from './local-variables.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HotkeysService, Hotkey } from 'angular2-hotkeys';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  activeModal;

  constructor(
    private toaster: ToastrService,
    public storage: LocalVariablesService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private _hotkeysService: HotkeysService,
    private modalService: NgbModal,
    // public network: NetworkService
  ) {

  }

  capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
  }

  addShortcutKey(controller){
    controller._hotkeysService.add(new Hotkey(['ctrl+a'], (event: KeyboardEvent, combo: string): ExtendedKeyboardEvent => {
      console.log('Combo: ' + combo);
      controller.fireAddView()
      const e: ExtendedKeyboardEvent = event;
      e.returnValue = false; // Prevent bubbling
      return e;
    }, ['input']));
  }

  cancelShortcutKey(controller){
    controller._hotkeysService.add(new Hotkey(['ctrl+x'], (event: KeyboardEvent, combo: string): ExtendedKeyboardEvent => {
      console.log('Combo: ' + combo);
      controller.onCancel()
      const e: ExtendedKeyboardEvent = event;
      e.returnValue = false; // Prevent bubbling
      return e;
    }, ['input']));
  }

  saveShortcutKey(controller){
    controller._hotkeysService.add(new Hotkey(['ctrl+s'], (event: KeyboardEvent, combo: string): ExtendedKeyboardEvent => {
      console.log('Combo: ' + combo);
      controller.onSubmit()
      const e: ExtendedKeyboardEvent = event;
      e.returnValue = false; // Prevent bubbling
      return e;
    }, ['input']));
  }

  getLoadingImage(){
    const restro = JSON.parse(this.storage.get('restro'))[0];
    return restro['loading_image'] ? restro['loading_image'] : 'assets/img/loader.gif';
    
  }

  isAdminEdit() {
    const restro = JSON.parse(this.storage.get('restro'))[0];
    return restro['admin_edit_layout'] ? restro['admin_edit_layout'] : '0';
  }

  getTncAccepted(){ 
    const restro = JSON.parse(this.storage.get('restro'))[0];
    return restro['is_tnc_accepted'] ? restro['is_tnc_accepted'] : '0';
  }

  restaurantId() {
    const restro = JSON.parse(this.storage.get('restro'))[0];
    return restro['restaurant_id'] ? restro['restaurant_id'] : '1';
  }

  allowTableMenuToShow(){
    const restro = JSON.parse(this.storage.get('restro'))[0];
    const pikdish_feature = restro['pikdish_feature'] ? restro['pikdish_feature'] : '-1';
    return (pikdish_feature == '0' || pikdish_feature == '1' || pikdish_feature == '2');
  }

  showLoader() {

  }

  hideLoader() {

  }

  presentSuccessToast(msg) {
    this.toaster.success(msg)
  }

  presentFailureToast(msg) {
    this.toaster.error(msg);
  }


}


