import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilityService } from './utility.service'
import { ApiService } from './api.service'
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch'
import { Observable, forkJoin, from } from 'rxjs';
import swal from 'sweetalert2';
import { reject } from 'q';


declare var require: any;
const tnc_json: any = require('../../shared/data/tnc.html');

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  constructor(public http: HttpClient,
    public utility: UtilityService,
    public api: ApiService) { 

    }

    getList(node, data){

      let link = ''
      switch(node){
        case 'tables':
          link = 'RestroTableList'
          break;
        case 'departments':
          link = 'RestroDepartmentList'
          break;
        case 'portions':
          link = 'RestroPortionList'
          break;
        case 'addons':
          link = 'RestroAddOnList'
            break;
        case 'toppings':
          link = 'RestroToppingList'
            break;
        case 'categories':
          link = "RestroCategoryList"
            break;
        default:
          break;
      }

      return this.httpPostResponse(link, data, null, true)

    }

    delete(node, data){

      let link = ''
      switch(node){
        case 'tables':
          link = 'DeleteRestroTable'
          break;
        case 'departments':
          link = 'DeleteRestroDept'
          break;
        case 'portions':
          link = 'DeleteRestroPortion'
          break;
        case 'addons':
          link = 'DeleteRestroAddOn'
            break;
        case 'toppings':
          link = 'DeleteRestroTopping'
            break;
        default:
          break;
      }

      return this.httpPostResponse(link, data, null, true)

    }

    addList(node, data){

      let link = ''
      switch(node){
        case 'tables':
          link = 'AddRestroTable'
          break;
        case 'departments':
          link = 'AddRestroDept'
          break;
        case 'portions':
          link = 'AddRestroPortion'
          break;
        case 'addons':
          link = 'AddRestroAddOn'
            break;
        case 'toppings':
          link = 'AddRestroTopping'
            break;
        default:
          break;
      }

      return this.httpPostResponse(link, data, null, true)
    }

    getItem(node, data){

      let link = ''
      switch(node){
        case 'tables':
          link = 'GetEditRestroTable'
          break;
        case 'departments':
          link = 'GetEditRestroDept'
          break;
        case 'portions':
          link = 'GetEditRestroPortion'
          break;
        case 'addons':
          link = 'GetEditRestroAddOn'
            break;
        case 'toppings':
          link = 'GetEditRestroTopping'
            break;
        default:
          break;
      }

      return this.httpPostResponse(link, data, null, true)
    }

    setItem(node, data){

      let link = ''
      switch(node){
        case 'tables':
          link = 'SetEditRestroTable'
          break;
        case 'departments':
          link = 'SetEditRestroDept'
          break;
        case 'portions':
          link = 'SetEditRestroPortion'
          break;
        case 'addons':
          link = 'SetEditRestroAddOn'
            break;
        case 'toppings':
          link = 'SetEditRestroTopping'
            break;
        default:
          break;
      }

      return this.httpPostResponse(link, data, null, true)
    }

    // post requests -- start
    loginUser(data){
      return this.httpPostResponse('RestroLogin', data, null, true)
    }


    // cuisines api -- start
    cuisines(data){
      return this.httpPostResponse('CuisinesList', data, null, true)
    }

    restroCuisines(data){
      return this.httpPostResponse('RestroCuisinesList', data, null, true)
    }

    addRestroCuisines(data){
      return this.httpPostResponse('AddRestroCuisines', data, null, true)
    }
    // cuisines api -- end

    setTncAgree(data){
      return this.httpPostResponse('SetTnC_Agree', data, null, false)
    }




    // post requests -- ends

    httpGetUrlResponse(key, id = null, showloader = false){

      if(showloader == true){
        this.utility.showLoader();
      }
  
      let _id = (id) ? '/' + id : ''
      let url = key + _id;
      let seq = this.api.geturl(url).share()
  
      seq.subscribe((res: any) => {
  
        if(showloader == true){
          this.utility.hideLoader();
        }
        if(res['success'] != true){
          this.showFailure(res);
        }
      }, err => {
        if(showloader == true){
          this.utility.hideLoader();
        }
        this.showFailure(err)
  
      })
      // .add( () => {
      //   if(showloader == true){
      //     this.utility.hideLoader();
      //   }
      // });
  
      return seq
      .catch(this.handleError);
    }

    httpGetResponse(key, id = null, showloader = false){

      if(showloader == true){
        this.utility.showLoader();
      }
  
      let _id = (id) ? '/' + id : ''
      let url = key + _id;
      let seq = this.api.get(url).share()
  
      seq.subscribe((res: any) => {
  
        if(showloader == true){
          this.utility.hideLoader();
        }
        if(res['success'] != true){
          this.showFailure(res);
        }
      }, err => {
        if(showloader == true){
          this.utility.hideLoader();
        }
        this.showFailure(err)
  
      })
      // .add( () => {
      //   if(showloader == true){
      //     this.utility.hideLoader();
      //   }
      // });
  
      return seq
      .catch(this.handleError);
    }
  
    httpPostResponse(key, data, id = null, showloader = false){
  
      if(showloader == true){
        this.utility.showLoader();
      }
  
      let _id = (id) ? '/' + id : ''
      let url = key + _id;
      let seq = this.api.post(url, data).share()
  
      seq.subscribe((res: any) => {
        if(showloader == true){
          this.utility.hideLoader();
        }
        
        if(res && res['success'] == "false"){
          this.showFailure(res);
        }
      }, err => {
        if(showloader == true){
          this.utility.hideLoader();
        }
        
        this.showFailure(err)

      })
      // .add( () => {
      //     // if(showloader == true){
      //     //   this.utility.hideLoader();
      //     // }
      // });
  
      return seq
      .catch(this.handleError)
    }
  
    private handleError(error: any) {
      //this.utility.presentFailureToast(errMsg);
      console.log(error);
      return Observable.throw(error);
    }
  
  
    showSuccess(){
  
  
  
    }
  
    showFailure(err){
      this.utility.presentFailureToast(err['error'])
    }

    isTncAccepted(){
      var self = this;
      return new Promise( (resolve, reject) => {
        
        const isAccepted = this.utility.getTncAccepted();

        if(isAccepted == 0){
          swal({
            width: '70%',
            heightAuto: true,
            html: "<div class='tnc-content'>" + tnc_json + '</div>',
            allowOutsideClick: false,
            showCancelButton: true,
            confirmButtonText: 'Agree',
            cancelButtonText: 'Disagree',
            showLoaderOnConfirm: true,
            preConfirm: function (isConfirm) {
                return new Promise(function (_resolve, _reject) {
                  const params = { restaurant_id: '1', tnc_agree: '1'};
  
                    self.setTncAgree(params).subscribe( res => {
                      _resolve();
                    }, err => { _reject() })
                });
            }
          }).then( (response) => {
            resolve(response);
          }, err => {
            reject(err)
          });
        }else{
          resolve({value: true});
        }

        
      })
    }
}
